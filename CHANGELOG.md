# Changelog

## 0.2.4

- Fixed README

## 0.2.3

- Removed file properties

## 0.2.2

- Fixed file drop
- Extended file properties

## 0.2.1

- Updated readme

## 0.2.0

- Added handle for input element files
- Added folder based format support
- Added tests

## 0.1.1

- Changed default file uid
