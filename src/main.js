const createFileUidDefault = (file) => {
  return file.name.substr(0, file.name.lastIndexOf('.'))
}
const checkFileAlreadyUploadedDefault = (file) => { return false }
const getFileExtensionDefault = (file) => { return file.name.substr(file.name.lastIndexOf('.') + 1) }

class FilteredFileDrop {
  constructor(
    supportedFormats,
    supportedMultiFileFormats,
    createFileUid = createFileUidDefault,
    checkFileAlreadyUploaded = checkFileAlreadyUploadedDefault,
    getFileExtension = getFileExtensionDefault
  ) {
    this.supportedFormats = supportedFormats
    this.supportedMultiFileFormats = supportedMultiFileFormats
    this._createFileUid = createFileUid
    this._checkFileAlreadyUploaded = checkFileAlreadyUploaded
    this._getFileExtension = getFileExtension
  }

  async handleDropEvent(event) {
    event.preventDefault()
    this.files = []
    const promises = []
    for (const item of event.dataTransfer.items) {
      if (item.kind === 'file') {
        const entry = item.webkitGetAsEntry()
        promises.push(this._handleEntry(entry))
      }
    }
    await Promise.allSettled(promises)
    this._filterFiles()
    return this.files
  }

  handleInputElementFiles(files) {
    this.files = []
    for (const file of files) {
      var fullPath = file.name
      if (file.webkitRelativePath) {
        fullPath = file.webkitRelativePath
      }
      this._addFile(file, fullPath)
    }
    this._filterFiles()
    return this.files
  }

  async _handleEntry(entry, path) {
    path = path || ''
    if (entry.isFile) {
      const file = await this._fileAsPromise(entry)
      this._addFile(file, entry.fullPath)
    } else if (entry.isDirectory) {
      for await (const dirEntry of this._getEntriesAsAsyncIterator(entry)) {
        await this._handleEntry(dirEntry, path + entry.name + '/')
      }
    }
  }

  _addFile(file, fullPath) {
    file.uid = this._createFileUid(file)
    if (!this._checkFileAlreadyUploaded(file)) {
      const fileExtension = this._getFileExtension(file)
      const fileBase = file.name.substr(0, file.name.lastIndexOf(fileExtension) - 1)
      var newFile = {
        uid: file.uid,
        file: file,
        fullPath: fullPath,
        fileBase: fileBase,
        fileExtension: fileExtension,
        linkedFiles: [],
        parentFile: null
      }
      this.files.push(newFile)
    }
  }

  _filterFiles() {
    const alreadyAddedFolders = []
    for (const file of this.files) {
      if (Object.keys(this.supportedMultiFileFormats).includes(file.fileExtension)) {
        if (this._checkFolderFormat(file)) {
          const folder = this._getFolder(file)
          if (alreadyAddedFolders.includes(folder)) {
            file.ignore = true
            file.linkedFiles = []
          } else {
            file.parentFile = null
            file.linkedFiles = this.files.filter(
              f => (this.supportedMultiFileFormats[file.fileExtension].fileExtensions.includes(f.fileExtension) &&
                this._checkLinkedFile(file, f) && this._checkFolderMatch(f, folder))
            )
            for (const linkedFile of file.linkedFiles) {
              linkedFile.parentFile = file
            }
            if (folder.includes('/')) {
              file.folder = folder.split('/').at(-1)
            } else {
              file.folder = folder
            }
            alreadyAddedFolders.push(folder)
          }
        } else {
          if (file.linkedFiles.length === 0) {
            file.linkedFiles = this.files.filter(
              f => (this.supportedMultiFileFormats[file.fileExtension].fileExtensions.includes(f.fileExtension) &&
                this._checkLinkedFile(file, f))
            )
          }
          for (const linkedFile of file.linkedFiles) {
            linkedFile.parentFile = file
          }
        }
      }
    }
    this.files = this.files.filter(f => this.supportedFormats.includes(f.fileExtension) && !f.ignore)
    this._filterDuplicates()
  }

  async _fileAsPromise(entry) {
    return new Promise((resolve, reject) => {
      entry.file(resolve, reject)
    })
  }


  _getFolder(file) {
    return file.fullPath.substr(0, file.fullPath.lastIndexOf('/'))
  }

  _checkLinkedFile(parentFile, file) {
    if (Object.keys(this.supportedMultiFileFormats).includes(parentFile.fileExtension)) {
      return this.supportedMultiFileFormats[parentFile.fileExtension].isLinkedFile(parentFile, file)
    }
    return false
  }

  _checkFolderFormat(file) {
    if (Object.keys(this.supportedMultiFileFormats).includes(file.fileExtension)) {
      if (this.supportedMultiFileFormats[file.fileExtension].folderFormat) {
        return true
      }
    }
    return false
  }

  _checkFolderMatch(file, folder) {
    return this._getFolder(file) === folder
  }

  _filterDuplicates() {
    const uids = []
    function filterDuplicatesInternal(file) {
      if (!uids.includes(file.uid)) {
        uids.push(file.uid)
        return true
      }
      return false
    }
    this.files = this.files.filter(f => filterDuplicatesInternal(f))
  }

  async *_getEntriesAsAsyncIterator(dirEntry) {
    const reader = dirEntry.createReader()
    const getNextBatch = () => new Promise((resolve, reject) => {
      reader.readEntries(resolve, reject)
    })
    let entries = []
    do {
      entries = await getNextBatch()
      for (const entry of entries) {
        yield entry
      }
    } while (entries.length > 0)
  }
}

export default FilteredFileDrop
