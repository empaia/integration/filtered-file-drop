import FilteredFileDrop from './main';

// SINGLE FILE TESTS

test("single file filter and property test", () => {
    const filteredFileDrop = new FilteredFileDrop(["svs"], {})
    const files = [
        { name: "file.svs" },
        { name: "file.png" },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(1)
    const expectedProperties = ["uid", "file", "fullPath", "fileBase", "fileExtension", "linkedFiles", "parentFile"]
    for (const expectedProperty of expectedProperties)
        expect(filteredFiles[0]).toHaveProperty(expectedProperty)
});


test("many single files test", () => {
    function createFileUid(file) {
        if (file.webkitRelativePath) {
            return file.webkitRelativePath
        }
        return file.name
    }
    const filteredFileDrop = new FilteredFileDrop(["svs", "png"], {}, createFileUid)
    const files = [
        { name: "file.svs" },
        { name: "file.png" },
        { name: "file.png", webkitRelativePath: "folder1/file.png" },
        { name: "file.png", webkitRelativePath: "folder2/file.png" },
        { name: "file.png", webkitRelativePath: "folder3/file.png" },
        { name: "file.png", webkitRelativePath: "folder4/file.png" },
        { name: "file.png", webkitRelativePath: "folder1/folder1/file.png" },
        { name: "file.pdf" },
        { name: "file.pdf", webkitRelativePath: "folder4/file.pdf" },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(7)
});

test("already uploaded test", () => {
    createFileUid = (file) => {
        if (file.webkitRelativePath) {
            return file.webkitRelativePath
        }
        return file.name
    }
    checkFileAlreadyUploaded = (file) => {
        return true
    }
    const filteredFileDrop = new FilteredFileDrop(["svs", "png"], {}, createFileUid, checkFileAlreadyUploaded)
    const files = [
        { name: "file.svs" },
        { name: "file.png" },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(0)
});

test("extended file extension handling test", () => {
    createFileUid = (file) => {
        if (file.webkitRelativePath) {
            return file.webkitRelativePath
        }
        return file.name
    }
    checkFileAlreadyUploaded = (file) => {
        return false
    }
    getFileExtension = (file) => {
        if (file.name.includes('.ome.')) {
            return file.name.substr(file.name.lastIndexOf('.ome.') + 1)
        } else {
            return file.name.substr(file.name.lastIndexOf('.') + 1)
        }
    }
    const filteredFileDrop = new FilteredFileDrop(["tif", "ome.tif"], {}, createFileUid, checkFileAlreadyUploaded, getFileExtension)
    const files = [
        { name: "file.tif" },
        { name: "file.ome.tif" },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(2)
    const expectedFileExtensions = {
        "file.tif": "tif",
        "file.ome.tif": "ome.tif"
    }
    for (const filteredFile of filteredFiles) {
        expect(filteredFile.fileExtension).toBe(expectedFileExtensions[filteredFile.file.name])
    }
});

test("duplicate uid filter test", () => {
    function createFileUid(file) {
        return "all the same"
    }
    const filteredFileDrop = new FilteredFileDrop(["svs", "png"], {}, createFileUid)
    const files = [
        { name: "file.svs" },
        { name: "file.png" },
        { name: "file.png", webkitRelativePath: "folder1/file.png" },
        { name: "file.png", webkitRelativePath: "folder2/file.png" },
        { name: "file.png", webkitRelativePath: "folder3/file.png" },
        { name: "file.png", webkitRelativePath: "folder4/file.png" },
        { name: "file.png", webkitRelativePath: "folder1/folder1/file.png" },
        { name: "file.pdf" },
        { name: "file.pdf", webkitRelativePath: "folder4/file.pdf" },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(1)
});


// MULTI FILE TESTS

getFolder = (file) => {
    return file.fullPath.substr(0, file.fullPath.lastIndexOf('/'))
}
const supportedFormats = ["svs", "dcm", "mrxs", "vsf"]
const supportedMultiFileFormats = {
    dcm: {
        fileExtensions: ['dcm'],
        isLinkedFile: function (parentFile, file) {
            return (file.fileExtension === 'dcm'
                && parentFile.uid !== file.uid
                && getFolder(file) === getFolder(parentFile))
        },
        folderFormat: true
    },
    mrxs: {
        fileExtensions: ['dat', 'ini'],
        isLinkedFile: function (parentFile, file) {
            var expectedLinkedFileFolder = parentFile.fileBase
            if (getFolder(parentFile)) {
                expectedLinkedFileFolder = getFolder(parentFile) + '/' + parentFile.fileBase
            }
            return getFolder(file) === expectedLinkedFileFolder
        }
    },
    vsf: {
        fileExtensions: ['img', 'jpg'],
        isLinkedFile: function (parentFile, file) {
            return (file.fileBase.includes(parentFile.fileBase)
                && getFolder(file) === getFolder(parentFile))
        }
    }
}


test("multi file test", () => {
    createFileUid = (file) => {
        const lastModifiedInSeconds = Math.floor(file.lastModified / 1000) * 1000
        return file.name + "-" + file.size + "-" + lastModifiedInSeconds
    }
    const filteredFileDrop = new FilteredFileDrop(supportedFormats, supportedMultiFileFormats, createFileUid)
    const files = [
        // SVS
        { name: "file.svs", size: 0, lastModified: 0, },
        // MRXS
        { name: "MAINFILE.mrxs", size: 0, lastModified: 0, },
        { name: "file.ini", webkitRelativePath: "MAINFILE/file.ini", size: 0, lastModified: 0, },
        { name: "file.dat", webkitRelativePath: "MAINFILE/file.dat", size: 0, lastModified: 0, },
        // DCM
        { name: "file0.dcm", webkitRelativePath: "folder_dcm/file0.dcm", size: 0, lastModified: 0, },
        { name: "file1.dcm", webkitRelativePath: "folder_dcm/file1.dcm", size: 0, lastModified: 0, },
        { name: "file2.dcm", webkitRelativePath: "folder_dcm/file2.dcm", size: 0, lastModified: 0, },
        // VSF
        { name: "vsf_name.vsf", webkitRelativePath: "folder_vsf/vsf_name.vsf", size: 0, lastModified: 0, },
        { name: "vsf_name-file.img", webkitRelativePath: "folder_vsf/vsf_name-file.img", size: 0, lastModified: 0, },
        { name: "vsf_name-file.jpg", webkitRelativePath: "folder_vsf/vsf_name-file.jpg", size: 0, lastModified: 0, },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(4)
    const expectedLinkedFilesCount = {
        "svs": 0,
        "mrxs": 2,
        "dcm": 2,
        "vsf": 2,
    }
    for (const filteredFile of filteredFiles) {
        expect(filteredFile.linkedFiles).toHaveLength(expectedLinkedFilesCount[filteredFile.fileExtension])
    }
});

test("multi file nested test", () => {
    createFileUid = (file) => {
        const lastModifiedInSeconds = Math.floor(file.lastModified / 1000) * 1000
        return file.name + "-" + file.size + "-" + lastModifiedInSeconds
    }
    const filteredFileDrop = new FilteredFileDrop(supportedFormats, supportedMultiFileFormats, createFileUid)
    const files = [
        // SVS
        { name: "file.svs", size: 0, lastModified: 0, },
        // SVS (same file in nested folder) -> should be ignored
        { name: "file.svs", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/file.svs", size: 0, lastModified: 0, },
        // SVS (different file with same name in nested folder) -> should be added
        { name: "file.svs", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/file.svs", size: 1, lastModified: 1, },
        // SVS (same file with different name in nested folder) -> should be added
        { name: "file1.svs", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/file1.svs", size: 0, lastModified: 0, },
        // MRXS
        { name: "MAINFILE.mrxs", size: 0, lastModified: 0, },
        { name: "file.ini", webkitRelativePath: "MAINFILE/file.ini", size: 0, lastModified: 0, },
        { name: "file.dat", webkitRelativePath: "MAINFILE/file.dat", size: 0, lastModified: 0, },
        // MRXS (same file in nested folder) -> should be ignored
        { name: "MAINFILE.mrxs", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/MAINFILE.mrxs", size: 0, lastModified: 0, },
        { name: "file.ini", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/MAINFILE/file.ini", size: 0, lastModified: 0, },
        { name: "file.dat", webkitRelativePath: "NESTED_FOLDER/NESTED_FOLDER/MAINFILE/file.dat", size: 0, lastModified: 0, },
        // MRXS (different file with same name in nested folder) -> should be added
        { name: "MAINFILE.mrxs", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE.mrxs", size: 1, lastModified: 1, },
        { name: "file.ini", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE/file.ini", size: 1, lastModified: 1, },
        { name: "file.dat", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE/file.dat", size: 1, lastModified: 1, },
        // MRXS (same file with different name in nested folder) -> should be added
        { name: "MAINFILE_NEW.mrxs", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE_NEW.mrxs", size: 0, lastModified: 0, },
        { name: "file.ini", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE_NEW/file.ini", size: 0, lastModified: 0, },
        { name: "file.dat", webkitRelativePath: "NESTED_FOLDER_1/NESTED_FOLDER/MAINFILE_NEW/file.dat", size: 0, lastModified: 0, },
        // DCM
        { name: "file0.dcm", webkitRelativePath: "folder_dcm/file0.dcm", size: 0, lastModified: 0, },
        { name: "file1.dcm", webkitRelativePath: "folder_dcm/file1.dcm", size: 0, lastModified: 0, },
        { name: "file2.dcm", webkitRelativePath: "folder_dcm/file2.dcm", size: 0, lastModified: 0, },
        // DCM (same file in nested folder) -> should be ignored
        { name: "file0.dcm", webkitRelativePath: "NESTED_FOLDER/folder_dcm/file0.dcm", size: 0, lastModified: 0, },
        { name: "file1.dcm", webkitRelativePath: "NESTED_FOLDER/folder_dcm/file1.dcm", size: 0, lastModified: 0, },
        { name: "file2.dcm", webkitRelativePath: "NESTED_FOLDER/folder_dcm/file2.dcm", size: 0, lastModified: 0, },
        // DCM (different file with same name in nested folder) -> should be added
        { name: "file0.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm/file0.dcm", size: 1, lastModified: 1, },
        { name: "file1.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm/file1.dcm", size: 1, lastModified: 1, },
        { name: "file2.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm/file2.dcm", size: 1, lastModified: 1, },
        // DCM (same file with different folder name in nested folder) -> should be ignored
        { name: "file0.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_1/file0.dcm", size: 0, lastModified: 0, },
        { name: "file1.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_1/file1.dcm", size: 0, lastModified: 0, },
        { name: "file2.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_1/file2.dcm", size: 0, lastModified: 0, },
        // DCM (same file with different name in nested folder) -> should be added
        { name: "file0_new.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_2/file1_new.dcm", size: 0, lastModified: 0, },
        { name: "file1_new.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_2/file1_new.dcm", size: 0, lastModified: 0, },
        { name: "file2_new.dcm", webkitRelativePath: "NESTED_FOLDER_1/folder_dcm_2/file2_new.dcm", size: 0, lastModified: 0, },
        // VSF
        { name: "vsf_name.vsf", webkitRelativePath: "folder_vsf/vsf_name.vsf", size: 0, lastModified: 0, },
        { name: "vsf_name-file.img", webkitRelativePath: "folder_vsf/vsf_name-file.img", size: 0, lastModified: 0, },
        { name: "vsf_name-file.jpg", webkitRelativePath: "folder_vsf/vsf_name-file.jpg", size: 0, lastModified: 0, },
        // VSF (same file in nested folder) -> should be ignored
        { name: "vsf_name.vsf", webkitRelativePath: "NESTED_FOLDER/folder_vsf/vsf_name.vsf", size: 0, lastModified: 0, },
        { name: "vsf_name-file.img", webkitRelativePath: "NESTED_FOLDER/folder_vsf/vsf_name-file.img", size: 0, lastModified: 0, },
        { name: "vsf_name-file.jpg", webkitRelativePath: "NESTED_FOLDER/folder_vsf/vsf_name-file.jpg", size: 0, lastModified: 0, },
        // VSF (different file with same name in nested folder) -> should be added
        { name: "vsf_name.vsf", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf/vsf_name.vsf", size: 1, lastModified: 1, },
        { name: "vsf_name-file.img", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf/vsf_name-file.img", size: 1, lastModified: 1, },
        { name: "vsf_name-file.jpg", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf/vsf_name-file.jpg", size: 1, lastModified: 1, },
        // VSF (same file with different name in nested folder) -> should be added
        { name: "vsf_name_new.vsf", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf_2/vsf_name_new.vsf", size: 0, lastModified: 0, },
        { name: "vsf_name_new-file.img", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf_2/vsf_name_new-file.img", size: 0, lastModified: 0, },
        { name: "vsf_name_new-file.jpg", webkitRelativePath: "NESTED_FOLDER_1/folder_vsf_2/vsf_name_new-file.jpg", size: 0, lastModified: 0, },
    ];
    const filteredFiles = filteredFileDrop.handleInputElementFiles(files)
    expect(filteredFiles).toHaveLength(12)
    const expectedLinkedFilesCount = {
        "svs": 0,
        "mrxs": 2,
        "dcm": 2,
        "vsf": 2,
    }
    console.log(filteredFiles)
    for (const filteredFile of filteredFiles) {
        expect(filteredFile.linkedFiles).toHaveLength(expectedLinkedFilesCount[filteredFile.fileExtension])
    }
});