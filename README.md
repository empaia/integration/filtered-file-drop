# Filtered File Drop

FilteredFileDrop can be used to handle file drop events and filter any kind of file and folder structure. It also supports grouping of multi-file formats.

## Usage

Example usage for WSI files, e.g. single-file svs, multi-file mrxs, multi-file dcm

```js
import FilteredFileDrop from '@empaia/filtered-file-drop'

getFolder = (file) => {
    return file.fullPath.substr(0, file.fullPath.lastIndexOf('/'))
}
const supportedFormats = ['svs', 'mrxs', 'dcm']
const supportedMultiFileFormats = {
    mrxs: {
        // expected file structure:
        // PARENTFILE.mrxs
        // PARENTFILE/LINKEDFILE.ini
        // PARENTFILE/LINKEDFILE.dat
        fileExtensions: ['dat', 'ini'],
        isLinkedFile: function (parentFile, file) {
            var expectedLinkedFileFolder = parentFile.fileBase
            if (getFolder(parentFile)) {
                expectedLinkedFileFolder = getFolder(parentFile) + '/' + parentFile.fileBase
            }
            return getFolder(file) === expectedLinkedFileFolder
        }
    },
    dcm: {
        // expected file structure:
        // FOLDER/PARENTFILE.dcm
        // FOLDER/LINKEDFILE.dcm
        fileExtensions: ['dcm'],
        isLinkedFile: function (parentFile, file) {
            return (file.fileExtension === 'dcm'
                && parentFile.uid !== file.uid
                && getFolder(file) === getFolder(parentFile))
        },
        folderFormat: true
    }
}

const filteredFileDrop = new FilteredFileDrop(
    supportedFormats,
    supportedMultiFileFormats
)
```

Now you can hand over a file drop event and get back the filtered files

```js
const files = await filteredFileDrop.handleDropEvent(dropEvent)
```

It is also possible to add files from an input element

```js
var inputElementFiles = document.getElementById(inputElementId).files
const files = await filteredFileDrop.handleInputElementFiles(inputElementFiles)
```
